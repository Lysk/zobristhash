/**
 * 
 */
package main;

import java.util.Random;

/**
 * @author Lysk
 *
 */
public class ZobristHash {
	long[][] table;
	/**
	 * For a 2d board
	 * @param len:   number of square (or positions: chess = 8*8, connect4= 6*7)
	 * @param width: number of state for a square (chess = 12 piece * 2 colors, connect4 = 2 players)
	 */
	public void init(int len, int width){
		Random rr = new Random();
		table = new long[len][width];
		for (int square = 0; square < table.length; square++){
			for (int side = 0; side < table[square].length; side++){
				table[square][side] = rr.nextLong();
			}
		}
	}

	public long computeKey(int[] board) {
		long hashKey = 0;
		for (int square = 0; square < board.length; square++){
			if (board[square] !=0 ) { // there is something on that square
				int side = board[square]-1;
				hashKey ^= table[square][side];
			}
		}
		return hashKey;
	}

	long updateKey(long oldKey, int moveSquare, int moveSide) {
		return oldKey ^ table[moveSquare][moveSide];
	}
}
