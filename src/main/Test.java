package main;

public class Test {

	public static void main(String[] args) {
		ZobristHash zh = new ZobristHash();
		zh.init(16, 2);
		int[] board = new int[]{0,0,0,0,
								0,0,0,0,
								0,0,0,0,
								0,1,2,0};
		System.out.println(zh.computeKey(board));
		board = new int[]{0,0,0,0,
				0,0,0,0,
				0,0,0,0,
				0,1,2,0};
		System.out.println(zh.computeKey(board));
	}
}


